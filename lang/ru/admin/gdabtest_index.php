<?php
$MESS['ABTEST_TITLE'] = 'АБ тесты';
$MESS['ABTEST_TITLE_ADD'] = 'Добавить тест';
$MESS['ABTEST_TITLE_EDIT'] = 'Редактирование теста';
$MESS['ABTEST_TITLE_ERROR_ELEMENT'] = 'Такого теста не существует';

$MESS['BTN_BACK'] = 'Назад';
$MESS['BTN_SAVE'] = 'Сохранить';
$MESS['BTN_DELETE'] = 'Удалить тест';
$MESS['BTN_EDIT'] = 'Изменить';
$MESS['BTN_ADD'] = 'Добавить тест';
$MESS['BTN_CREATE'] = 'Создать';

$MESS['SUCCESS_EDIT'] = 'Тест изменён';
$MESS['SUCCESS_ADD'] = 'Тест добавлен';
$MESS['SUCCESS_DELETE'] = 'Тест удалён';
$MESS['ERROR_DELETE'] = 'Не удалось удалить тест. Попробуйте ещё раз.';

$MESS['ERROR_CODE'] = 'Символьный код не должен быть пустым, а также может содержать только латинские буквы, цифры и символы нижнего подчёркивания';
$MESS['ERROR_PERCENT'] = 'Аудитория участников должно быть целое число от 1 до 100';
$MESS['ERROR_YMGOAL'] = 'Цель Яндекс может содержать только латинские буквы, цифры и символы нижнего подчёркивания';
$MESS['ERROR_COOKIE'] = 'Время жизни cookie должно быть целое число от 0';
$MESS['ERROR_DATE'] = 'Неверный формат даты';
$MESS['ERROR_EMPTY_TESTS'] = 'Сейчас нет тестов. Пожалуйста, добавьте.';

$MESS['FIELD_NUMBER'] = '№';
$MESS['FIELD_ID'] = 'Номер';
$MESS['FIELD_ACTIVE'] = 'Активность';
$MESS['FIELD_ACTIVE_DOUBLE'] = 'Актив-ность';
$MESS['FIELD_NAME'] = 'Название';
$MESS['FIELD_CODE'] = 'Символьный код';
$MESS['FIELD_PERCENT'] = 'Аудитория участников, %';
$MESS['FIELD_YMGOAL_A'] = 'Цель Яндекс вариант А';
$MESS['FIELD_YMGOAL_B'] = 'Цель Яндекс вариант B';
$MESS['FIELD_CREATE_DATE'] = 'Дата создания';
$MESS['FIELD_EDIT_DATE'] = 'Дата изменения';
$MESS['FIELD_STOP_DATE'] = 'Дата окончания';
$MESS['FIELD_EXECUTE_GOAL'] = 'Момент отправки цели';
$MESS['FIELD_EXECUTE_GOAL_I'] = 'При входе на сайт';
$MESS['FIELD_EXECUTE_GOAL_F'] = 'На странице с тестом';
$MESS['FIELD_LOG_WRITE'] = 'Вести логи';
$MESS['FIELD_EDIT'] = 'Редактировать';
$MESS['FOLDER_LOG'] = 'Папка логов';

$MESS['LINK_OPEN'] = 'Открыть';
