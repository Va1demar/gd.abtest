<?php
$MESS['ABTEST_TABLE_ACTIVE_TITLE'] = 'Активность';

$MESS['ABTEST_TABLE_NAME_TITLE'] = 'Название';
$MESS['ABTEST_TABLE_NAME_DEFAULT'] = 'Новый тест';

$MESS['ABTEST_TABLE_CODE_TITLE'] = 'Символьный код';
$MESS['ABTEST_TABLE_CODE_DEFAULT'] = 'new_test';

$MESS['ABTEST_TABLE_PERCENT_TITLE'] = 'Процент участников';

$MESS['ABTEST_TABLE_YMGOAL_A_TITLE'] = 'Цель Яндекс для варианта A';
$MESS['ABTEST_TABLE_YMGOAL_B_TITLE'] = 'Цель Яндекс для варианта B';

$MESS['ABTEST_TABLE_CREATE_DATE_TITLE'] = 'Дата начала теста';
$MESS['ABTEST_TABLE_EDIT_DATE_TITLE'] = 'Дата изменения теста';
$MESS['ABTEST_TABLE_STOP_DATE_TITLE'] = 'Дата окончания теста';

$MESS['ABTEST_TABLE_EXECUTE_GOAL_TITLE'] = 'Момент срабатывания цели';

$MESS['ABTEST_TABLE_LOG_WRITE_TITLE'] = 'Вести логи';
