<?php

namespace GD\ABTest;

use GD\ABTest\ABTestTable;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Web\Cookie;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class PublicHandler
{
    private static $instance = null;

    private function __construct()
    {
        self::$context = Application::getInstance()->getContext();
        self::$request = self::$context->getRequest();
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    static $arStopDerictory = ['/local/', '/ajax/', '/bitrix/', '/upload/'];

    static $context;

    static $request;

    static $prefixCookie = 'gd_abtest_';

    /**
     * Функция выполняемая по событию OnProlog
     *
     * @return void
     */
    public function init(): void
    {
        if (ADMIN_SECTION !== true) {
            $GLOBALS['GD_AB_TESTS'] = [];
            self::$context = Application::getInstance()->getContext();
            self::$request = self::$context->getRequest();

            if (!empty(self::getYandexCampaing())) {
                Asset::getInstance()->addString(self::getJsReachGoal());
            }

            $uri = new Uri(self::$request->getRequestUri());
            $path = $uri->getPath();
            if (!self::isStopDerictory($path)) {
                if (!empty($arActiveTests = self::getTestStartPage())) {
                    $arGoals = [];
                    foreach ($arActiveTests as $arTest) {
                        $cookieName = self::$prefixCookie . $arTest['CODE'];
                        $cookieValue = self::$request->getCookie($cookieName);
                        if ($cookieValue === 'A' || $cookieValue === 'B') {
                            $GLOBALS["GD_AB_TESTS"][$arTest['CODE']] = $cookieValue;
                            self::setCookie($cookieName, $cookieValue, $arTest['STOP_DATE']);
                            continue;
                        } else {
                            if (self::isPartyTest($arTest['PERCENT'])) {
                                $variant = self::generateVariant();
                                if (!empty($arTest['YMGOAL_A']) && !empty($arTest['YMGOAL_B'])) {
                                    switch ($variant) {
                                        case 'A':
                                            $arGoals[] = $arTest['YMGOAL_A'];
                                            break;
                                        case 'B':
                                            $arGoals[] = $arTest['YMGOAL_B'];
                                            break;
                                    }
                                }
                            } else {
                                $variant = 'A';
                            }
                            self::setCookie($cookieName, $variant, $arTest['STOP_DATE']);
                            $GLOBALS['GD_AB_TESTS'][$arTest['CODE']] = $variant;
                            if ($arTest['LOG_WRITE'] === 'Y') {
                                self::writeLog($arTest['CODE'], $variant);
                            }
                        }
                    }

                    if (!empty($arGoals) && !empty(self::getYandexCampaing())) {
                        $stringGoals = "";
                        foreach ($arGoals as $goal) {
                            $stringGoals .= self::createStringGoal($goal);
                        }
                        Asset::getInstance()->addString('<script type="text/javascript">document.addEventListener("DOMContentLoaded", function() {' . $stringGoals . '}, false);</script>');
                    }
                }

                if (!empty($arDeactiveTests = self::getDeactiveTests())) {
                    foreach ($arDeactiveTests as $arDeacTest) {
                        $GLOBALS['GD_AB_TESTS'][$arDeacTest['CODE']] = 'A';
                    }
                }
            }
        }
    }

    /**
     * Проверка на запрещенные разделы сайта (ajax подключения)
     *
     * @param  string $path путь страницы
     * @return bool
     */
    private function isStopDerictory(string $path): bool
    {
        $arStopDerictory = self::$arStopDerictory;
        foreach ($arStopDerictory as $dir) {
            if (strpos($path, $dir) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * Получаем все актуальные тесты, которые выполняются на старте любой страницы
     *
     * @return array
     */
    private function getTestStartPage(): array
    {
        $arResult = [];
        $arOrder = ['ID' => 'ASC'];
        $arFilter = [
            'ACTIVE' => 'Y',
            'EXECUTE_GOAL' => 'I',
            '>STOP_DATE' => new DateTime(),
        ];
        $rsObj = ABTestTable::getList([
            'order' => $arOrder,
            'filter' => $arFilter,
            'cache' => [
                'ttl' => 3600,
                'cache_joins' => true
            ],
        ]);
        while ($arr = $rsObj->fetch()) {
            $arResult[] = $arr;
        }
        if (!$arResult) {
            return [];
        }
        return $arResult;
    }

    /**
     * Получить неактивные тесты
     *
     * @return array
     */
    private function getDeactiveTests(): array
    {
        $arResult = [];
        $arOrder = ['ID' => 'ASC'];
        $arFilter = [
            'LOGIC' => 'OR',
            ['!ACTIVE' => 'Y'],
            ['<=STOP_DATE' => new DateTime()],
        ];
        $rsObj = ABTestTable::getList([
            'order' => $arOrder,
            'filter' => $arFilter,
            'cache' => [
                'ttl' => 3600,
                'cache_joins' => true
            ],
        ]);
        while ($arr = $rsObj->fetch()) {
            $arResult[] = $arr;
        }

        if (!$arResult) {
            return [];
        }

        return $arResult;
    }


    /**
     * Установка куки
     *
     * @param  string $nameCookie Имя куки
     * @param  string $valueCookie Значение куки
     * @param  DateTime $stopDate Дата окончания теста
     * @return void
     */
    private function setCookie(string $nameCookie, string $valueCookie, DateTime $stopDate): void
    {

        $cookie = new Cookie($nameCookie, $valueCookie, $stopDate->getTimestamp());
        $cookie->setSecure(false);
        $cookie->setHttpOnly(false);
        self::$context->getResponse()->addCookie($cookie);
    }

    /**
     * Генерация варианта теста
     *
     * @return string
     */
    private function generateVariant(): string
    {
        $rand = mt_rand(1, 100);
        if ($rand > 50) {
            return 'B';
        }
        return 'A';
    }

    /**
     * Является ли пользователь участником теста
     *
     * @param  int $percentGroup Процент аудитории, которая участвует в тесте
     * @return bool
     */
    private function isPartyTest(int $percentGroup): bool
    {
        $rand = mt_rand(1, 100);
        if ($rand <= $percentGroup) {
            return true;
        }
        return false;
    }

    /**
     * Номер кампании Яндекс
     *
     * @return string
     */
    private function getYandexCampaing(): string
    {
        return Option::get('gd.abtest', 'ya_campaign');
    }

    /**
     * Функция для выполнения целей Яндекса
     *
     * @return string
     */
    private function getJsReachGoal(): string
    {
        return '<script type="text/javascript">
            const ymReachGoalGD = (nameGoal) => {
                const PM_YM_COUNTER = ' . self::getYandexCampaing() . ';
                let ee = setInterval(function () {
                    if (typeof window.ym != "undefined") {
                        ym(PM_YM_COUNTER, "reachGoal", nameGoal);
                        clearInterval(ee);
                    } else {
                        console.log("Metrika find...");
                    }
                }, 1000);
            }
        </script>';
    }

    /**
     * Возвращает js строку
     *
     * @param  string $goal Цель Яндекс
     * @return string
     */
    private function createStringGoal(string $goal): string
    {
        return 'ymReachGoalGD("' . $goal . '");';
    }

    /**
     * Получить вариант по коду
     *
     * @param  string $code
     * @return string
     */
    public function getVariantForCode(string $code): string
    {
        if (isset($GLOBALS['GD_AB_TESTS'][$code])) {
            return $GLOBALS['GD_AB_TESTS'][$code];
        }

        if (empty($arTest = self::getTestForCode($code))) {
            $GLOBALS['GD_AB_TESTS'][$code] = 'A';
            return 'A';
        }

        $cookieName = self::$prefixCookie . $code;
        $cookieValue = self::$request->getCookie($cookieName);

        if ($cookieValue === 'A' || $cookieValue === 'B') {
            $this->setCookie($cookieName, $cookieValue, $arTest['STOP_DATE']);
            $GLOBALS['GD_AB_TESTS'][$code] = $cookieValue;
            return $cookieValue;
        }

        if ($this->isPartyTest($arTest['PERCENT'])) {
            $variant = $this->generateVariant();
            $this->setCookie($cookieName, $variant, $arTest['STOP_DATE']);
            $GLOBALS['GD_AB_TESTS'][$code] = $variant;
            if (!empty($arTest['YMGOAL_A']) && !empty($arTest['YMGOAL_B'])) {
                switch ($variant) {
                    case 'A':
                        $stringGoal = $this->createStringGoal($arTest['YMGOAL_A']);
                        break;
                    case 'B':
                        $stringGoal = $this->createStringGoal($arTest['YMGOAL_B']);
                        break;
                    default:
                        $stringGoal = '';
                        break;
                }
                Asset::getInstance()->addString('<script type="text/javascript">document.addEventListener("DOMContentLoaded", function() {' . $stringGoal . '}, false);</script>');
                if ($arTest['LOG_WRITE'] === 'Y') {
                    $this->writeLog($arTest['CODE'], $variant);
                }
                return $variant;
            } else {
                return $variant;
            }
        }

        $GLOBALS['GD_AB_TESTS'][$code] = 'A';
        return 'A';
    }

    /**
     * Получить активный тест, который выполняется непосредственно в момент определения варианта 
     *
     * @param  string $code Код теста
     * @return array
     */
    private function getTestForCode(string $code): array
    {
        $arOrder = ['ID' => 'ASC'];
        $arFilter = [
            'ACTIVE' => 'Y',
            'CODE' => $code,
            'EXECUTE_GOAL' => 'F',
            '>STOP_DATE' => new DateTime(),
        ];
        $arResult = ABTestTable::getList([
            'order' => $arOrder,
            'filter' => $arFilter,
            'cache' => [
                'ttl' => 3600,
                'cache_joins' => true
            ],
        ])->fetch();

        if (!$arResult) {
            return [];
        }

        return $arResult;
    }

    /**
     * Делает запись в файле лога
     *
     * @param  string $code
     * @param  string $variant
     * @return void
     */
    private function writeLog(string $code, string $variant)
    {
        $date = date('d_m_y');
        $hour = date('H');
        $dirPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/gd.abtest/logs/' . $code . '/' . $date . '/';
        if (!file_exists($dirPath)) {
            mkdir($dirPath, 0777, true);
        }
        $filePath = $dirPath . $hour . '.csv';
        if (!file_exists($filePath)) {
            $logFile = fopen($filePath, 'a');
            fputcsv($logFile, self::getHeadFile(), ';');
        } else {
            $logFile = fopen($filePath, 'a');
        }
        $arResult = [
            $variant,
            $_SERVER['REMOTE_ADDR'],
            self::getPhpSession(),
            $_SERVER['HTTP_USER_AGENT'],
        ];
        fputcsv($logFile, $arResult, ';');
        fclose($logFile);
    }

    /**
     * Получить заголовки для файла логов
     *
     * @return array
     */
    private function getHeadFile(): array
    {
        return [
            Loc::getMessage('ABTEST_HEAD_FILE_VARIANT'),
            Loc::getMessage('ABTEST_HEAD_FILE_IP'),
            Loc::getMessage('ABTEST_HEAD_FILE_PHP_SESSION'),
            Loc::getMessage('ABTEST_HEAD_FILE_BROWSER'),
        ];
    }

    /**
     * Получить текущую сессию php
     *
     * @return string
     */
    private function getPhpSession(): string
    {
        if ($id = session_id()) {
            return $id;
        }
        return '0';
    }
}
