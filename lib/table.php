<?php

namespace GD\ABTest;

use Bitrix\Main\Entity\Validator;
use Bitrix\Main\Entity\DataManager;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class ABTestTable extends DataManager
{

    /**
     * Возвращает имя таблицы
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'gd_abtest';
    }

    /**
     * Возвращает массив с полями таблицы
     *
     * @return array
     */
    public static function getMap(): array
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ],
            'ACTIVE' => [
                'data_type' => 'boolean',
                'title' => Loc::getMessage('ABTEST_TABLE_ACTIVE_TITLE'),
                'values' => ['N', 'Y'],
                'required' => true,
                'default_value' => function () {
                    return 'Y';
                },
            ],
            'NAME' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('ABTEST_TABLE_NAME_TITLE'),
                'default_value' => function () {
                    return Loc::getMessage('ABTEST_TABLE_NAME_DEFAULT');
                },
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
                'required'  => false
            ],
            'CODE' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('ABTEST_TABLE_CODE_TITLE'),
                'default_value' => function () {
                    return Loc::getMessage('ABTEST_TABLE_CODE_DEFAULT');
                },
                'validation' => function () {
                    return [
                        new Validator\Length(null, 255),
                    ];
                },
                'required'  => true
            ],
            'PERCENT' => [
                'data_type' => 'integer',
                'title' => Loc::getMessage('ABTEST_TABLE_PERCENT_TITLE'),
                'default_value' => function () {
                    return 100;
                },
                'required'  => true
            ],
            'YMGOAL_A' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('ABTEST_TABLE_YMGOAL_A_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 100),
                    ];
                },
                'required'  => false
            ],
            'YMGOAL_B' => [
                'data_type' => 'string',
                'title' => Loc::getMessage('ABTEST_TABLE_YMGOAL_B_TITLE'),
                'validation' => function () {
                    return [
                        new Validator\Length(null, 100),
                    ];
                },
                'required'  => false
            ],
            'CREATE_DATE' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('ABTEST_TABLE_CREATE_DATE_TITLE'),
                'required'  => true
            ],
            'EDIT_DATE' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('ABTEST_TABLE_EDIT_DATE_TITLE'),
                'required'  => true
            ],
            'STOP_DATE' => [
                'data_type' => 'datetime',
                'title' => Loc::getMessage('ABTEST_TABLE_STOP_DATE_TITLE'),
                'required'  => true
            ],
            'EXECUTE_GOAL' => [
                'data_type' => 'enum',
                'title' => Loc::getMessage('ABTEST_TABLE_EXECUTE_GOAL_TITLE'),
                'values' => ['I', 'F'],
                'default_value' => function () {
                    return 'I';
                },
                'required' => true
            ],
            'LOG_WRITE' => [
                'data_type' => 'boolean',
                'title' => Loc::getMessage('ABTEST_TABLE_LOG_WRITE_TITLE'),
                'values' => ['N', 'Y'],
                'default_value' => function () {
                    return 'N';
                },
                'required'  => false
            ],
        ];
    }
}
