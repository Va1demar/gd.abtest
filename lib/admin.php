<?php

namespace GD\ABTest;

use  GD\ABTest\ABTestTable;
use  Bitrix\Main\Type\DateTime;

class AdminHandler
{
    private static $instance = null;

    private function __construct()
    {

    }

    public static function getInstance()
	{
		if (null === self::$instance)
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

    /**
     * Получить всю таблицу с АБ тестами в виде массива
     *
     * @return array
     */
    public function getTable(): array
    {
        $arResult = [];
        $rsObj = ABTestTable::getList(['order' => ['ID' => 'ASC']]);
        while ($arr = $rsObj->fetch()) {
            $arr['CREATE_DATE'] = $this->convertDateToString($arr['CREATE_DATE']);
            $arr['EDIT_DATE'] = $this->convertDateToString($arr['EDIT_DATE']);
            $arr['STOP_DATE'] = $this->convertDateToString($arr['STOP_DATE']);
            $arr['ACTIVE_STATUS'] = $this->isActiveTest($arr);
            $arResult[] = $arr;
        }
        return $arResult;
    }

    /**
     * Преобразует объект класса Bitrix\Main\Type\DateTime в строку
     *
     * @param  DateTime $date
     * @return string
     */
    private function convertDateToString(DateTime $date): string
    {
        $dateString = '';
        if ($date && ($date instanceof DateTime)) {
            return $date->format('d.m.Y H:i:s');
        }
        return $dateString;
    }

    /**
     * Получить одну запись по ID
     *
     * @param  int $id
     * @return array
     */
    public function getOneTest(int $id): array
    {
        $arResult = [];
        $arFilter = ['ID' => $id];
        $rsObj = ABTestTable::getList(['filter' => $arFilter]);
        while ($arr = $rsObj->fetch()) {
            $arr['STOP_DATE'] = $this->convertDateToString($arr['STOP_DATE']);
            $arResult = $arr;
        }
        return $arResult;
    }

    /**
     * Проверка полей на корректность и возвращение массива ошибок
     *
     * @param  array $arFields
     * @return array
     */
    public function validations(array $arFields): array
    {
        $arErrors = [];
        if (!empty($error = $this->checkCode($arFields['CODE']))) {
            $arErrors[] = $error;
        }
        if (!empty($error = $this->checkPercent($arFields['PERCENT']))) {
            $arErrors[] = $error;
        }
        if (!empty($error = $this->checkYmgoal($arFields['YMGOAL_A'])) || !empty($error = $this->checkYmgoal($arFields['YMGOAL_B']))) {
            $arErrors[] = $error;
        }
        if (!empty($error = $this->checkDate($arFields['STOP_DATE']))) {
            $arErrors[] = $error;
        }
        return $arErrors;
    }

    /**
     * Проверка символьного кода на корректность
     *
     * @param  string $value Значение
     * @return string
     */
    private function checkCode(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return 'ERROR_CODE';
        }
        if (iconv_strlen($value) < 2 && iconv_strlen($value) > 100) {
            return 'ERROR_CODE';
        }
        if (preg_match('/[^A-Za-z0-9-_]/', $value)) {
            return 'ERROR_CODE';
        }
        return '';
    }

    /**
     * Проверка процентов на корректность
     *
     * @param  string $value Значение
     * @return string
     */
    private function checkPercent(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return '';
        }
        if (($value = filter_var($value, FILTER_VALIDATE_INT)) === false) {
            return 'ERROR_PERCENT';
        }
        if ($value < 1 || $value > 100) {
            return 'ERROR_PERCENT';
        }
        return '';
    }

    /**
     * Проверка цели Яндекс на корректность
     *
     * @param  string $value Значение
     * @return string
     */
    private function checkYmgoal(string $value): string
    {
        $value = trim($value);
        if (empty($value)) {
            return '';
        }
        if (preg_match('/[^A-Za-z0-9-_]/', $value)) {
            return 'ERROR_YMGOAL';
        }
        return '';
    }

    /**
     * Проверка даты на корректность
     *
     * @param  string $value
     * @return string
     */
    private function checkDate(string $value): string
    {
        $value = trim($value);
        try {
            new DateTime($value);
        } catch (\Exception $e) {
            return 'ERROR_DATE';
        }
        return '';
    }

    /**
     * Преобразование данных для записи
     *
     * @param  array $arData
     * @return array
     */
    public function convertData(array $arData): array
    {
        $arResult = [];
        $arResult['ACTIVE'] = $arData['ACTIVE'] == 'Y' ? 'Y' : 'N';
        $arResult['NAME'] = isset($arData['NAME']) ? trim($arData['NAME']) : '';
        $arResult['CODE'] = trim($arData['CODE']);
        $arResult['PERCENT'] = !empty($arData['PERCENT']) ? (int)trim($arData['PERCENT']) : 100;
        $arResult['YMGOAL_A'] = isset($arData['YMGOAL_A']) ? trim($arData['YMGOAL_A']) : '';
        $arResult['YMGOAL_B'] = isset($arData['YMGOAL_B']) ? trim($arData['YMGOAL_B']) : '';
        $arResult['STOP_DATE'] = !empty($arData['STOP_DATE']) ? new DateTime($arData['STOP_DATE']) : DateTime::createFromTimestamp(strtotime("+14 days"));
        $arResult['EXECUTE_GOAL'] = isset($arData['EXECUTE_GOAL']) ? $arData['EXECUTE_GOAL'] : 'I';
        $arResult['LOG_WRITE'] = $arData['LOG_WRITE'] == 'Y' ? 'Y' : 'N';
        return $arResult;
    }

    /**
     * Обновляет запись и возвращает массив ошибок
     *
     * @param  int $id
     * @param  array $arData
     * @return array
     */
    public function editTest(int $id, array $arData): array
    {
        $arErrors = [];
        $arData["EDIT_DATE"] = new DateTime();
        $result = ABTestTable::update($id, $arData);
        if (!$result->isSuccess()) {
            $arErrors = $result->getErrorMessages();
        }
        return $arErrors;
    }

    /**
     * Создаёт запись в таблице и возвращает массив ошибок
     *
     * @param  array $arData
     * @return array
     */
    public function addTest(array $arData): array
    {
        $arErrors = [];
        $arData["CREATE_DATE"] = new DateTime();
        $arData["EDIT_DATE"] = new DateTime();
        $result = ABTestTable::add($arData);
        if (!$result->isSuccess()) {
            $arErrors = $result->getErrorMessages();
        }
        return $arErrors;
    }

    /**
     * Удаляет тест из таблицы
     *
     * @param  int $id Первичный ключ записи
     * @return array
     */
    public function deleteTest(int $id): array
    {
        $arErrors = [];
        $result = ABTestTable::delete($id);
        if (!$result->isSuccess()) {
            $arErrors = $result->getErrorMessages();
        }
        return $arErrors;
    }
    
    /**
     * Проверка активности теста
     *
     * @param  array $data
     * @return string
     */
    private function isActiveTest(array $data): string
    {
        if ($data['ACTIVE'] != 'Y') {
            return 'stop';
        }
        $unixTimeStop = strtotime($data['STOP_DATE']);
        $unixTimeNow = time();
        if ($unixTimeStop > $unixTimeNow) {
            if ($unixTimeStop - $unixTimeNow <= 86400 * 3) {
                return 'attention';
            } else {
                return 'active';
            }
        } else {
            return 'stop';
        }        
    }
}
