<?php
Bitrix\Main\Loader::registerAutoloadClasses(
    "gd.abtest",
    [
        "GD\\ABTest\\ABTestTable" => "lib/table.php",
        "GD\\ABTest\\AdminHandler" => "lib/admin.php",
        "GD\\ABTest\\PublicHandler" => "lib/public.php",
    ]
);
