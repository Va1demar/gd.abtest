<?php

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Base;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\EventManager;

use GD\ABTest\ABTestTable;

IncludeModuleLangFile(__FILE__);

class gd_abtest extends CModule
{
    public function __construct()
    {
        $arModuleVersion = array();
        include __DIR__ . '/version.php';
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_ID = 'gd.abtest';
        $this->MODULE_NAME = Loc::getMessage('ABTEST_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('ABTEST_MODULE_DESCRIPTION');

        $this->MODULE_GROUP_RIGHTS = 'N';

        $this->PARTNER_NAME = Loc::getMessage('ABTEST_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('ABTEST_PARTNER_URI');
    }

    public function doInstall()
    {
        ModuleManager::registerModule($this->MODULE_ID);
        $this->installFiles();
        $this->installDB();
        if (Loader::includeModule($this->MODULE_ID)) {
            EventManager::getInstance()->registerEventHandler(
                "main",
                "OnProlog",
                $this->MODULE_ID,
                "\GD\ABTest\PublicHandler",
                "init"
            );
        }
    }

    function doUninstall()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            EventManager::getInstance()->unRegisterEventHandler(
                "main",
                "OnProlog",
                $this->MODULE_ID,
                "\GD\ABTest\PublicHandler",
                "init"
            );
        }
        $this->uninstallFiles();
        $this->uninstallDB();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    function installDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            ABTestTable::getEntity()->createDbTable();
        }
    }

    function uninstallDB()
    {
        if (Loader::includeModule($this->MODULE_ID)) {
            if (Application::getConnection()->isTableExists(Base::getInstance('\GD\ABTest\ABTestTable')->getDBTableName())) {
                $connection = Application::getInstance()->getConnection();
                $connection->dropTable(ABTestTable::getTableName());
            }
        }
    }

    function installEvents()
    {
        return true;
    }

    function unInstallEvents()
    {
        return true;
    }

    function installFiles()
    {
        copyDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/admin/',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin',
            true,
            true
        );
        return true;
    }

    function uninstallFiles()
    {
        deleteDirFiles(
            $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/admin/',
            $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin'
        );
        return true;
    }
}
