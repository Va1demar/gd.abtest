<?php

use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
use Bitrix\Main\Page\Asset;
use GD\ABTest\ABTestTable;
use GD\ABTest\AdminHandler;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';
if (!Loader::includeModule('gd.abtest')) return false;

Loc::loadMessages(__FILE__);

$module_id = pathinfo(dirname(__DIR__))['basename'];

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

Asset::getInstance()->addString('<link rel="stylesheet" href="' . getLocalPath('modules/' . $module_id . '/admin/css/style.min.css') . '">');

if ($_GET['type'] == 'edit' && (int)$_GET['id'] > 0) {
    require_once __DIR__ . '/include/edit.php';
} elseif ($_GET['type'] == 'remove' && (int)$_GET['id'] > 0) {
    require_once __DIR__ . '/include/delete.php';
} elseif ($_GET['type'] == 'add') {
    require_once __DIR__ . '/include/add.php';
} else {
    switch ($_GET['type']) {
        case 'successEdit':
            echo (\CAdminMessage::ShowNote(Loc::getMessage('SUCCESS_EDIT')));
            break;
        case 'successAdd':
            echo (\CAdminMessage::ShowNote(Loc::getMessage('SUCCESS_ADD')));
            break;
        case 'successDelete':
            echo (\CAdminMessage::ShowNote(Loc::getMessage('SUCCESS_DELETE')));
            break;
        case 'errorDelete':
            echo (\CAdminMessage::ShowMessage(Loc::getMessage('ERROR_DELETE')));
            break;
    }
    require_once __DIR__ . '/include/view.php';
}

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';
