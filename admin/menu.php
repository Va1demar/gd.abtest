<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$aMenu = array(
    array(
        'parent_menu' => 'global_menu_marketing',
        'sort' => 1000,
        'text' => Loc::getMessage('ABTEST_TITLE'),
        'title' => Loc::getMessage('ABTEST_TEXT'),
        'url' => 'gdabtest_index.php?lang=' . LANGUAGE_ID,
        'icon' => 'abtest_menu_icon',
        'page_icon' => 'abtest_menu_icon',
        'items_id' => 'menu_references'
    )
);
return $aMenu;
