<?php

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;
use GD\ABTest\AdminHandler;

$instAdminHandler = AdminHandler::getInstance();

global $APPLICATION;
$APPLICATION->SetTitle(Loc::getMessage('ABTEST_TITLE_EDIT'));

$arRes = $instAdminHandler->getOneTest((int)$_GET['id']);

$request = Application::getInstance()->getContext()->getRequest();
$uri = new Uri($request->getRequestUri());
$uri->deleteParams(['id', 'type']);

echo '<a href="' . $uri->getUri() . '" class="gdabtest__btn-back">' . Loc::getMessage('BTN_BACK') . '</a>';

if (filter_var($_POST['ID'], FILTER_VALIDATE_INT) && $_POST['SAVE_EDIT'] == 'Y') {
    if (!empty($arError = $instAdminHandler->validations($_POST))) {
        foreach ($arError as $error) {
            echo (\CAdminMessage::ShowMessage(Loc::getMessage($error)));
        }
    } else {
        if (!empty($arErrorsEdit = $instAdminHandler->editTest($_POST['ID'], $instAdminHandler->convertData($_POST)))) {
            foreach ($arErrorsEdit as $error) {
                echo (\CAdminMessage::ShowMessage($error));
            }
        } else {
            $uri->addParams(['type' => 'successEdit']);
            $urlSuccess = $uri->getUri();
            LocalRedirect($urlSuccess);
        }
    }
}
if (!empty($arRes)) {
?>
    <form method="post" class="gdabtest__form" name="FORM_EDIT">
        <input type="hidden" name="ID" value="<?= $arRes['ID'] ?>">
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_ID') ?></div>
            <div class="gdabtest__entry-input">
                <?= $_GET['id']; ?>
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_ACTIVE') ?></div>
            <div class="gdabtest__entry-input">
                <input name="ACTIVE" type="checkbox" value="Y" <?= $arRes['ACTIVE'] == 'Y' ? 'checked' : '' ?>>
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_NAME') ?></div>
            <div class="gdabtest__entry-input">
                <input name="NAME" type="text" value="<?= $arRes['NAME'] ?>">
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_CODE') ?></div>
            <div class="gdabtest__entry-input">
                <input name="CODE" type="text" value="<?= $arRes['CODE'] ?>">
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_PERCENT') ?></div>
            <div class="gdabtest__entry-input">
                <input name="PERCENT" type="text" value="<?= $arRes['PERCENT'] ?>">
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_YMGOAL_A') ?></div>
            <div class="gdabtest__entry-input">
                <input name="YMGOAL_A" type="text" value="<?= $arRes['YMGOAL_A'] ?>">
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_YMGOAL_B') ?></div>
            <div class="gdabtest__entry-input">
                <input name="YMGOAL_B" type="text" value="<?= $arRes['YMGOAL_B'] ?>">
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_STOP_DATE') ?></div>
            <div class="gdabtest__entry-input">
                <?= CalendarDate("STOP_DATE", $arRes['STOP_DATE'], "", "17", "") ?>
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_EXECUTE_GOAL') ?></div>
            <div class="gdabtest__entry-input">
                <div>
                    <input type="radio" id="exgoal1" name="EXECUTE_GOAL" value="I" <?= $arRes['EXECUTE_GOAL'] == 'I' ? 'checked' : '' ?>>
                    <label for="exgoal1"><?= Loc::getMessage('FIELD_EXECUTE_GOAL_I') ?></label>
                </div>
                <div>
                    <input type="radio" id="exgoal2" name="EXECUTE_GOAL" value="F" <?= $arRes['EXECUTE_GOAL'] == 'F' ? 'checked' : '' ?>>
                    <label for="exgoal2"><?= Loc::getMessage('FIELD_EXECUTE_GOAL_F') ?></label>
                </div>
            </div>
        </div>
        <div class="gdabtest__entry">
            <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_LOG_WRITE') ?></div>
            <div class="gdabtest__entry-input">
                <input name="LOG_WRITE" type="checkbox" value="Y" <?= $arRes['LOG_WRITE'] == 'Y' ? 'checked' : '' ?>>
            </div>
        </div>
        <div class="gdabtest__btn-block">
            <input type="hidden" name="SAVE_EDIT" value="Y">
            <input type="submit" value="<?= Loc::getMessage('BTN_SAVE') ?>">
            <?
            $uri->addParams(['type' => 'remove', 'id' => $arRes['ID']]);
            $urlRemove = $uri->getUri();
            ?>
            <a href="<?=$urlRemove?>" class="gdabtest__btn-remove"><?= Loc::getMessage('BTN_DELETE') ?></a>
        </div>
    </form>
<?
} else {
    echo (\CAdminMessage::ShowMessage(Loc::getMessage("ABTEST_TITLE_ERROR_ELEMENT")));
}
