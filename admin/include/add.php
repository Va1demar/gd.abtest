<?php

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;
use GD\ABTest\AdminHandler;

$instAdminHandler = AdminHandler::getInstance();

global $APPLICATION;
$APPLICATION->SetTitle(Loc::getMessage("ABTEST_TITLE_ADD"));

$request = Application::getInstance()->getContext()->getRequest();
$uri = new Uri($request->getRequestUri());
$uri->deleteParams(['type']);
echo '<a href="' . $uri->getUri() . '" class="gdabtest__btn-back">' . Loc::getMessage('BTN_BACK') . '</a>';

if ($_POST['CREATE_TEST'] == 'Y') {
    if (!empty($arError = $instAdminHandler->validations($_POST))) {
        foreach ($arError as $error) {
            echo (\CAdminMessage::ShowMessage(Loc::getMessage($error)));
        }
    } else {
        if (!empty($arErrorsEdit = $instAdminHandler->addTest($instAdminHandler->convertData($_POST)))) {
            foreach ($arErrorsEdit as $error) {
                echo (\CAdminMessage::ShowMessage($error));
            }
        } else {
            $uri->addParams(['type' => 'successAdd']);
            $urlSuccess = $uri->getUri();
            LocalRedirect($urlSuccess);
        }
    }
}
?>

<form method="post" class="gdabtest__form" name="FORM_CREATE">
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_ACTIVE') ?></div>
        <div class="gdabtest__entry-input">
            <input name="ACTIVE" type="checkbox" value="Y" checked>
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_NAME') ?></div>
        <div class="gdabtest__entry-input">
            <input name="NAME" type="text" value="">
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_CODE') ?></div>
        <div class="gdabtest__entry-input">
            <input name="CODE" type="text" value="">
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_PERCENT') ?></div>
        <div class="gdabtest__entry-input">
            <input name="PERCENT" type="text" value="100">
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_YMGOAL_A') ?></div>
        <div class="gdabtest__entry-input">
            <input name="YMGOAL_A" type="text" value="">
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_YMGOAL_B') ?></div>
        <div class="gdabtest__entry-input">
            <input name="YMGOAL_B" type="text" value="">
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_STOP_DATE') ?></div>
        <div class="gdabtest__entry-input">
            <?= CalendarDate("STOP_DATE", "", "", "17", "") ?>
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_EXECUTE_GOAL') ?></div>
        <div class="gdabtest__entry-input">
            <div>
                <input type="radio" id="exgoal1" name="EXECUTE_GOAL" value="I" checked>
                <label for="exgoal1"><?= Loc::getMessage('FIELD_EXECUTE_GOAL_I') ?></label>
            </div>
            <div>
                <input type="radio" id="exgoal2" name="EXECUTE_GOAL" value="F">
                <label for="exgoal2"><?= Loc::getMessage('FIELD_EXECUTE_GOAL_F') ?></label>
            </div>
        </div>
    </div>
    <div class="gdabtest__entry">
        <div class="gdabtest__entry-title"><?= Loc::getMessage('FIELD_LOG_WRITE') ?></div>
        <div class="gdabtest__entry-input">
            <input name="LOG_WRITE" type="checkbox" value="Y">
        </div>
    </div>
    <div class="gdabtest__btn-block">
        <input type="hidden" name="CREATE_TEST" value="Y">
        <input type="submit" value="<?= Loc::getMessage('BTN_CREATE') ?>">
    </div>
</form>