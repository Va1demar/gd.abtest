<?php

use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;
use GD\ABTest\AdminHandler;

$instAdminHandler = AdminHandler::getInstance();

$request = Application::getInstance()->getContext()->getRequest();
$uri = new Uri($request->getRequestUri());
$uri->deleteParams(['type', 'id']);

if (empty($arErrors = $instAdminHandler->deleteTest((int)$_GET['id']))) {
    $uri->addParams(['type' => 'successDelete']);
} else {
    $uri->addParams(['type' => 'errorDelete']);
}

$url = $uri->getUri();
LocalRedirect($url);
