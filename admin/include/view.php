<?php

use Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;
use GD\ABTest\AdminHandler;

global $APPLICATION;
$APPLICATION->SetTitle(Loc::getMessage("ABTEST_TITLE"));

$arRes = AdminHandler::getInstance()->getTable();

$request = Application::getInstance()->getContext()->getRequest();
$uri = new Uri($request->getRequestUri());

if (!empty($arRes)) {
?>
    <div class="gdabtest__wrap-table">
        <div class="gdabtest__head">
            <div class="gdabtest__NUMBER"><?= Loc::getMessage("FIELD_NUMBER") ?></div>
            <div class="gdabtest__ACTIVE"><?= Loc::getMessage("FIELD_ACTIVE_DOUBLE") ?></div>
            <div class="gdabtest__NAME"><?= Loc::getMessage("FIELD_NAME") ?></div>
            <div class="gdabtest__CODE"><?= Loc::getMessage("FIELD_CODE") ?></div>
            <div class="gdabtest__PERCENT"><?= Loc::getMessage("FIELD_PERCENT") ?></div>
            <div class="gdabtest__YMGOAL_A"><?= Loc::getMessage("FIELD_YMGOAL_A") ?></div>
            <div class="gdabtest__YMGOAL_B"><?= Loc::getMessage("FIELD_YMGOAL_B") ?></div>
            <div class="gdabtest__CREATE_DATE"><?= Loc::getMessage("FIELD_CREATE_DATE") ?></div>
            <div class="gdabtest__EDIT_DATE"><?= Loc::getMessage("FIELD_EDIT_DATE") ?></div>
            <div class="gdabtest__STOP_DATE"><?= Loc::getMessage("FIELD_STOP_DATE") ?></div>
            <div class="gdabtest__EXECUTE_GOAL"><?= Loc::getMessage("FIELD_EXECUTE_GOAL") ?></div>
            <div class="gdabtest__LOG_WRITE"><?= Loc::getMessage("FIELD_LOG_WRITE") ?></div>
            <div class="gdabtest__FOLDER_LOG"><?= Loc::getMessage("FOLDER_LOG") ?></div>
            <div class="gdabtest__EDIT"><?= Loc::getMessage("FIELD_EDIT") ?></div>
        </div>
        <? foreach ($arRes as $key => $value) : ?>
            <div class="gdabtest__data">
                <div class="gdabtest__NUMBER <?= $value['ACTIVE_STATUS'] ?>"><?= $key + 1 ?></div>
                <div class="gdabtest__ACTIVE"><input type="checkbox" <?= $value['ACTIVE'] == 'Y' ? 'checked' : '' ?> onclick="return false"></div>
                <div class="gdabtest__NAME"><?= $value['NAME'] ?></div>
                <div class="gdabtest__CODE"><?= $value['CODE'] ?></div>
                <div class="gdabtest__PERCENT"><?= $value['PERCENT'] ?></div>
                <div class="gdabtest__YMGOAL_A"><?= $value['YMGOAL_A'] ?></div>
                <div class="gdabtest__YMGOAL_B"><?= $value['YMGOAL_B'] ?></div>
                <div class="gdabtest__CREATE_DATE"><?= $value['CREATE_DATE'] ?></div>
                <div class="gdabtest__EDIT_DATE"><?= $value['EDIT_DATE'] ?></div>
                <div class="gdabtest__STOP_DATE"><?= $value['STOP_DATE'] ?></div>
                <?
                $executeGoal = "";
                if ($value['EXECUTE_GOAL'] == 'I') {
                    $executeGoal = Loc::getMessage("FIELD_EXECUTE_GOAL_I");
                } elseif ($value['EXECUTE_GOAL'] == 'F') {
                    $executeGoal = Loc::getMessage("FIELD_EXECUTE_GOAL_F");
                }
                ?>
                <div class="gdabtest__EXECUTE_GOAL"><?= $executeGoal ?></div>
                <div class="gdabtest__LOG_WRITE"><input type="checkbox" <?= $value['LOG_WRITE'] == 'Y' ? 'checked' : '' ?> onclick="return false"></div>
                <div class="gdabtest__FOLDER_LOG">
                    <?
                    $pathLog = '/upload/gd.abtest/logs/' . $value['CODE'] . '/';
                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . $pathLog)) {
                        echo '<a href="/bitrix/admin/fileman_admin.php?lang=ru&site=s1&path=' . $pathLog . '" target="_blank">' . Loc::getMessage("LINK_OPEN") . '</a>';
                    }
                    ?>
                </div>
                <?
                $uri->addParams(['type' => 'edit', 'id' => $value['ID']]);
                $urlEdit = $uri->getUri();
                ?>
                <div class="gdabtest__EDIT"><a href="<?= $urlEdit ?>"><?= Loc::getMessage("BTN_EDIT") ?></a></div>
            </div>
        <? endforeach; ?>
    </div>
<?
} else {
?>
    <div class="adm-info-message-wrap">
        <div class="adm-info-message">
            <?= Loc::getMessage("ERROR_EMPTY_TESTS") ?>
        </div>
    </div>
<?
}

$uri->addParams(['type' => 'add']);
$uri->deleteParams(['id']);
$urlAdd = $uri->getUri();
?>
<div class="gdabtest__new-test-btn">
    <a href="<?= $urlAdd ?>"><?= Loc::getMessage("BTN_ADD") ?></a>
</div>